<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Book;

class HomeController extends Controller
{
    public function index()
    {
    	$data = DB::table("books")->get();
    	if($data->count() > 0)
    	{
    		return view("index",["index" => $data]);
    	}else{
    		abort(404);
    	}
    	
    }

    public function detail($slug)
    {
    	$data = DB::table("books")->where("slug",$slug)->get();
    	return view("detail",["data" => $data]);
    }
}
