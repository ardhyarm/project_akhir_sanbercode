<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Models\User;

class AuthController extends Controller
{
    public function index()
    {
    	return view("Auth.index");
    }

    public function login(Request $request)
    {
        $request->validate([
            "username" => "required",
            "password" => "required"
            ]);

        $credentials = ["name" => $request->username, "password" => $request->password];
    
        if(!Auth::attempt($credentials))
        {
            return redirect("/Auth");
        }else{
            return redirect("/admin");
        }
    }

    public function logout(Request $request)
    {
    Auth::logout();

    $request->session()->invalidate();

    $request->session()->regenerateToken();

    return redirect('/Auth');
    }
}
