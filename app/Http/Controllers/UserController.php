<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
    	$user = DB::table("users")->get();
    	return view("admin.user.index",["user" => $user]);
    }

    public function create_user()
    {
    	return view("admin.user.create_user");
    }

    public function store(Request $request)
    {
    	DB::table("users")->insert(
    		["name" => $request->name,
    		"email" => $request->email,
    		"password" => Hash::make($request->password)]);
    	return redirect("/admin/user");
    }

    public function delete($id)
    {
    	DB::table("users")->where("id",$id)->delete();
    	return redirect("/admin/user");
    }

    public function edit($id)
    {
    	$user = DB::table("users")->where("id",$id)->get();
    	return view("admin.user.edit",["user" => $user]);
    }

    public function update(Request $request)
    {
     	DB::table("users")->where("id",$request->id)->insert(
    		["name" => $request->name,
    		"password" => Hash::make($request->password)]);
    	return redirect("/admin/user");   	
    }
}
