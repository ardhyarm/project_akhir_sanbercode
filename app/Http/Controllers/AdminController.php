<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\slug;
use Illuminate\Support\Str;
use App\Models\Book;

class AdminController extends Controller
{
    public function index()
    {
    	$data = DB::table("books")->get();
    	return view("admin.index",["data" => $data]);
    }

    public function book()
    {
    	return view("admin.create_book");
    }

    public function store(Request $request)
    {
    	// validasi
    	$request->validate(
    		["nama" => "required",
             "gambar" => "required",
    		 "penulis" => "required",
    		 "deskripsi" => "required",
    		 "persediaan" => "required",
    		 "tautan" => "required",
    	]);

    	// insert data
    	DB::table("books")->insert([
    		"nama" => $request->nama,
    		"slug" => Str::slug($request->nama,"-"),
            "gambar" => $request->gambar,
    		"penulis" => $request->penulis,
    		"deskripsi" => $request->deskripsi,
    		"persediaan" => $request->persediaan,
    		"tautan" => $request->tautan
    	]);

    	return redirect("/admin");

    }

    public function edit($id)
    {
    	$data = DB::table("books")->where("id",$id)->get();
    	return view("admin.edit",["data" => $data]);
    }

    public function update(Request $request)
    {
    	// validasi
    	$request->validate(
    		["nama" => "required",
    		 "penulis" => "required",
             "gambar" => "required",
    		 "deskripsi" => "required",
    		 "persediaan" => "required",
    		 "tautan" => "required",
    	]);

    	// update data
    	DB::table("books")->where("id",$request->id)->update([
    		"nama" => $request->nama,
    		"slug" => Str::slug($request->nama,"-"),
            "gambar" => $request->gambar,
    		"penulis" => $request->penulis,
    		"deskripsi" => $request->deskripsi,
    		"persediaan" => $request->persediaan,
    		"tautan" => $request->tautan
    	]);

    	return redirect("/admin");
    }

    public function delete($id)
    {
    	DB::table("books")->where("id",$id)->delete();
    	return redirect("/admin");
    }
}
