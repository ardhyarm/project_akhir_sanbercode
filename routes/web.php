<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get("/","HomeController@index");
Route::get("/book/{slug}","HomeController@detail");

Route::get("/Auth","AuthController@index");
Route::post("/Auth/login","AuthController@login");
Route::get("/Auth/logout","AuthController@logout");

Route::group(["middleware" => "auth"], function(){
Route::get("/admin","AdminController@index");
Route::get("/admin/create_book","AdminController@book");
Route::post("/admin/add_book","AdminController@store");
Route::get("/admin/edit/{id}","AdminController@edit");
Route::get("/admin/delete/{id}","AdminController@delete");
Route::post("/admin/update_book","AdminController@update");
Route::get("/admin/user","UserController@index");
Route::get("/admin/user/create_user","UserController@create_user");
Route::post("/admin/user/create","UserController@store");
Route::get("/admin/user/delete/{id}","UserController@delete");
Route::get("/admin/user/edit/{id}","UserController@edit");
Route::post("/admin/user/update","UserController@update");
});