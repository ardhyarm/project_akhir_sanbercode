<!DOCTYPE html>
<html>
<head>
	<title>Admin - Add Book</title>
</head>
<body>

	@foreach($data as $dt)
	<form action="/admin/update_book" method="POST">
		@csrf
		<input type="hidden" name="id" value="{{$dt->id}}">
		<br>
		<label>
			Gambar :
		</label>
		<br>
		<input type="text" name="gambar" value="{{$dt->gambar}}">
		<br>
		<label>
			Nama Buku :
		</label>
		<br>
		<input type="text" name="nama" value="{{$dt->nama}}">
		<br>
		<label>
			Nama Penulis :
		</label>
		<br>
		<input type="text" name="penulis" value="{{$dt->penulis}}">
		<br>
		<label>
			Deskripsi :
		</label>
		<br>
		<textarea name="deskripsi">{{$dt->deskripsi}}</textarea>
		<br>
		<label>
			Persediaan :
		</label>
		<br>
		<input type="number" name="persediaan" value="{{$dt->persediaan}}">
		<br>
		<label>
			Tautan :
		</label>
		<br>
		<input type="text" name="tautan" value="{{$dt->tautan}}">
		<br><br>
		<input type="submit" name="add" value="add">
	</form>
	@endforeach

</body>
</html>